using System;
using System.Collections;
using System.Threading.Tasks;
using MediatR;
using Microsoft.AspNetCore.Mvc;
using TechnicalTest.Controllers;
using Xunit;
using Moq;
using System.Net;

namespace TechnicalTest.UnitTest
{
    public class AccountsControllerTest
    {
        AccountsController _accountsController;
        Mock<IMediator> _mediator;
        public AccountsControllerTest()
        {
            _mediator = new Mock<IMediator>();
            _accountsController = new AccountsController(_mediator.Object);
        }

        [Fact]
        public async Task Get_WithValidCustomerId_ReturnBalance()
        {
            int customerId = 1;

            var accountInfo = await _accountsController.Get(customerId);

            Assert.Equal(accountInfo.Value, 0);
        }



        [Fact]
        public async Task Get_WithInValidCustomerId_ReturnBalance()
        {
            int customerId = 0;

            var result = await _accountsController.Get(customerId);

            Assert.Equal((int)HttpStatusCode.BadRequest, (result.Result as ObjectResult)?.StatusCode);
        }


        [Fact]
        public async Task Deposit_Return_OkResult()
        {
            int customerId = 1;

            var result = await _accountsController.Deposit(customerId, 50);

            Assert.Equal((int)HttpStatusCode.OK, (result as OkResult)?.StatusCode);
        }


        [Fact]
        public async Task Withdraw_Return_OkResult()
        {
            int customerId = 1;

            var result = await _accountsController.Withdraw(customerId, 50);

            Assert.Equal((int)HttpStatusCode.OK, (result as OkResult)?.StatusCode);
        }

        [Fact]
        public async Task Transfer_Return_OkResult()
        {
            var transferDto = new DataTransferObjects.AccountDTOs.TransferDto
            {
                From = 1,
                Funds = 50,
                To = 2
            };

            var result = await _accountsController.Post(transferDto);

            Assert.Equal((int)HttpStatusCode.OK, (result as OkResult)?.StatusCode);
        }

    }
}

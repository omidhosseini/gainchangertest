using System;
using System.Collections.Generic;
using Moq;
using Xunit;

namespace Repository.UnitTest
{
    public class InMemoryRepository
    {

        Mock<IRepository> _inMemoryRepository;
        public InMemoryRepository()
        {
            _inMemoryRepository = new Mock<IRepository>();
        }

        [Fact]
        public void WithdrawFunds_NegativeBalance_ThrowException()
        {
            int customerId = 1;
            decimal funds = 50;
            _inMemoryRepository.Setup(x => x.WithdrawFunds(customerId, funds))
                                .Throws(new Exception("There is not enough balance in the account"));

            try
            {
                _inMemoryRepository.Object.WithdrawFunds(customerId, funds);
                Assert.True(false);
            }
            catch (System.Exception)
            {
                Assert.True(true);
            }
        }
    }
}

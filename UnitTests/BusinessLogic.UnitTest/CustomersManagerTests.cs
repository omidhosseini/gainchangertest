using System.Collections.Generic;
using System.Linq;
using DataTransferObjects;
using Moq;
using Xunit;

namespace BusinessLogic.UnitTest
{
    public class CustomersManagerTests
    {
        List<CustomerDto> customersMock = new List<CustomerDto>(){
            new CustomerDto{
                Id = 1,
                Name = "Customer 1",
                Surname = "Customer 1",
                IdCard = "IdCard 1"
            },
            new CustomerDto{
                Id = 2,
                Name = "Customer 1",
                Surname = "Customer 1",
                IdCard = "IdCard 1"
                },
        };

        Mock<ICustomersManager> _customerManager;
        public CustomersManagerTests()
        {
            _customerManager = new Mock<ICustomersManager>();

        }

        [Fact]
        public void GetCustomer_ReturnCustomer()
        {
            int customerId = 1;
            _customerManager.Setup(X => X.GetCustomer(customerId))
                     .Returns((int id) => customersMock.FirstOrDefault(x => x.Id == id));

            var customer = _customerManager.Object.GetCustomer(customerId);

            Assert.Equal(customersMock.FirstOrDefault(), customer);
        }

        [Fact]
        public void GetCustomer_ReturnCustomers()
        {
            int pageSize = 10;
            int pageNumber = 0;
            _customerManager.Setup(X => X.GetCustomers(pageSize, pageNumber))
                     .Returns(() => customersMock.Skip(pageSize * pageNumber).Take(pageSize).ToList());

            var customer = _customerManager.Object.GetCustomers(pageSize, pageNumber);

            Assert.Equal(customersMock, customer);
        }
    }
}
using System;
using Xunit;
using Moq;
using Repository;

namespace BusinessLogic.UnitTest
{
    public class AccountServiceTests
    {
        Mock<IAccountService> _accountService;
        public AccountServiceTests()
        {
            _accountService = new Mock<IAccountService>();
            
            _accountService.Setup(x => x.GetAvailableFunds(It.IsAny<Int32>()))
                            .Returns((double i) => 50m);
        }
        [Fact]
        public void GetAvailableFunds_ReturnBalance()
        {
            Int32 customerId = 1;

            var result = _accountService.Object.GetAvailableFunds(customerId);

            Assert.Equal(50m, result);
        }
    }
}

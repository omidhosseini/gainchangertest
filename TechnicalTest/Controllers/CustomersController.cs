﻿namespace TechnicalTest.Controllers
{
    using System;
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using BusinessLogic.Commands.Customers;
    using BusinessLogic.Queries.Customers;
    using DataTransferObjects;
    using MediatR;
    using Microsoft.AspNetCore.Mvc;

    [Route("api/customers")]
    [ApiController]
    public class CustomersController : ControllerBase
    {
        #region Fields
        /// <summary>
        /// The Mediator
        /// </summary>
        private readonly IMediator _mediator;
        #endregion

        #region Constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="CustomersController" /> class.
        /// </summary>
        /// <param name="customersManager">The customers manager.</param>
        public CustomersController(IMediator mediator)
        {
            _mediator = mediator;
        }

        #endregion

        #region Methods

        // DELETE api/customers/5
        [HttpDelete("{id}")]
        public async Task Delete(Int32 id)
        {
            await _mediator.Send(new DeleteCustomerCommand(id));
        }

        // GET api/customers
        [HttpGet("{pageSize}/{pageNumber}")]
        public async Task<ActionResult<IEnumerable<CustomerDto>>> Get(int pageSize, int pageNumber)
        {
            var customers = await _mediator.Send(new GetCustomersQuery(pageSize, pageNumber));

            return this.Ok(customers);
        }

        // GET api/customers/5
        [HttpGet("{id}")]
        public async Task<ActionResult<String>> Get(Int32 id)
        {
            CustomerDto customer = await _mediator.Send(new GetCustomerQuery(id));
            return this.Ok(customer);
        }

        // POST api/customers
        [HttpPost]
        public async Task Post([FromBody] CustomerDto customer)
        {
            await _mediator.Send(new SaveCustomerCommand(customer));
        }

        // PUT api/customers/5
        [HttpPut("{id}")]
        public async Task Put([FromBody] CustomerDto customer)
        {
            await _mediator.Send(new SaveCustomerCommand(customer));
        }

        #endregion
    }
}
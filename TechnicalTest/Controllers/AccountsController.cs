using System.Threading.Tasks;
using BusinessLogic;
using BusinessLogic.Commands.Account;
using BusinessLogic.Queries.Account;
using DataTransferObjects.AccountDTOs;
using MediatR;
using Microsoft.AspNetCore.Mvc;

namespace TechnicalTest.Controllers
{
    [Route("api/accounts")]
    [ApiController]
    public class AccountsController : ControllerBase
    {
        object lockObj = new object();
        private readonly IMediator _mediator;
        public AccountsController(IMediator mediator)
        {
            _mediator = mediator;
        }

        // GET /api/accounts/1
        [HttpGet("{Id}")]
        public async Task<ActionResult<decimal>> Get(int Id)
        {
            if (Id <= 0)
                return BadRequest("Invalid Id");

            var accountInfo = await _mediator.Send(new GetAvailableFundsQuery(Id));
            return Ok(accountInfo);
        }

        // POST /api/accounts/1/deposit
        [HttpPost("{customerId}/deposit")]
        public async Task<ActionResult> Deposit([FromRoute] int customerId, [FromBody] decimal funds)
        {
            await _mediator.Send(new DepositFundsCommand(customerId, funds));
            return Ok();
        }

        // POST /api/accounts/1/withdraw
        [HttpPost("{customerId}/withdraw")]
        public async Task<ActionResult> Withdraw([FromRoute] int customerId, [FromBody] decimal funds)
        {
            await _mediator.Send(new WithdrawFundsCommand(customerId, funds));
            return Ok();
        }

        // POST /api/accounts/transfer
        [HttpPost("transfer")]
        public async Task<ActionResult> Post([FromBody] TransferDto transferDto)
        {
            await _mediator.Send(new DepositFundsCommand(transferDto.From, transferDto.Funds));
            await _mediator.Send(new WithdrawFundsCommand(transferDto.To, transferDto.Funds));

            return Ok();
        }
    }
}
namespace DataTransferObjects.AccountDTOs
{
    public class TransferDto
    {
        public int From { get; set; }
        public int To { get; set; }
        public decimal Funds { get; set; }
    }
}
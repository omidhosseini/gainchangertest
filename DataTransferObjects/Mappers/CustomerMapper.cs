using System.Collections.Generic;
using System.Linq;
using Domain.EntityModels;

namespace DataTransferObjects.Mappers
{
    public static class CustomerMapper
    {
        public static CustomerDto ToDto(this CustomerModel dataModel)
        {
            if (dataModel is null)
                return new CustomerDto();

            return new CustomerDto
            {
                Id = dataModel.Id,
                IdCard = dataModel.IdCard,
                Name = dataModel.Name,
                Surname = dataModel.Surname
            };
        }


        public static IEnumerable<CustomerDto> ToDto(this IEnumerable<CustomerModel> dataModel)
        {
            return dataModel.Select(x => new CustomerDto
            {
                Id = x.Id,
                IdCard = x.IdCard,
                Name = x.Name,
                Surname = x.Surname
            });
        }

        public static CustomerModel ToDataModel(this CustomerDto dataModel)
        {
            if (dataModel is null)
                return new CustomerModel();

            return new CustomerModel
            {
                Id = dataModel.Id,
                IdCard = dataModel.IdCard,
                Name = dataModel.Name,
                Surname = dataModel.Surname
            };
        }
    }
}
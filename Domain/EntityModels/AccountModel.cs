namespace Domain.EntityModels
{
    public class AccountModel
    {
        public int Id { get; set; }

        public int CustomerId { get; set; }        
        public CustomerModel CustomerInfo { get; set; }
    }
}
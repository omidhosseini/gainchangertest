using DataTransferObjects;
using MediatR;

namespace BusinessLogic.Queries.Customers
{
    public class GetCustomerQuery : IRequest<CustomerDto>
    { 
        public int CustomerId { get; }
        public GetCustomerQuery(int customerId)
        {
            this.CustomerId = customerId;

        }
    }
}
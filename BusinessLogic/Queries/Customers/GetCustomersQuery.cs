using System.Collections.Generic;
using DataTransferObjects;
using MediatR;

namespace BusinessLogic.Queries.Customers
{
    public class GetCustomersQuery : IRequest<IEnumerable<CustomerDto>>
    {

        public int PageSize { get; }
        public int PageNumber { get; }
        
        public GetCustomersQuery(int pageSize, int pageNumber)
        {
            this.PageSize = pageSize;
            this.PageNumber = pageNumber;
        }
    }
}
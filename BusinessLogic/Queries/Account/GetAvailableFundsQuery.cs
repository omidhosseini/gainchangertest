using MediatR;

namespace BusinessLogic.Queries.Account
{
    public class GetAvailableFundsQuery : IRequest<decimal>
    {
        public int CustomerId { get; }
        public GetAvailableFundsQuery(int customerId)
        {
            this.CustomerId = customerId;
        }
    }
}
﻿namespace BusinessLogic
{
    using System;
    using System.Collections.Generic;
    using DataTransferObjects;

    public interface ICustomersManager
    {
        #region Methods

        /// <summary>
        /// Deletes the customer.
        /// </summary>
        /// <param name="id">The identifier.</param>
        void DeleteCustomer(Int32 id);

        /// <summary>
        /// Gets the customer.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns></returns>
        CustomerDto GetCustomer(Int32 id);

        /// <summary>
        /// Adds the customer.
        /// </summary>
        /// <param name="customer">The customer.</param>
        void SaveCustomer(CustomerDto customer);
        IEnumerable<CustomerDto> GetCustomers(int pageSize, int pageNumber);

        #endregion
    }
}
using System.Threading;
using System.Threading.Tasks;
using DataTransferObjects.AccountDTOs;
using Repository;

namespace BusinessLogic
{
    public class AccountService : IAccountService
    {
        private readonly IRepository _repository;

        public AccountService(IRepository repository)
        {
            _repository = repository;
        }

        public void DepositFunds(int customerId, decimal funds)
        {
            _repository.DepositFunds(customerId, funds);
        }

        public decimal GetAvailableFunds(int customerId)
        {
            return _repository.GetAvailableFunds(customerId);
        }

        public void WithdrawFunds(int customerId, decimal funds)
        {
            _repository.WithdrawFunds(customerId, funds);
        }
    }
}
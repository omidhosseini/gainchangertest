using System;

namespace BusinessLogic
{
    public interface IAccountService
    {
        Decimal GetAvailableFunds(Int32 customerId);
        void DepositFunds(Int32 customerId, Decimal funds);

        void WithdrawFunds(Int32 customerId, Decimal funds);
    }
}
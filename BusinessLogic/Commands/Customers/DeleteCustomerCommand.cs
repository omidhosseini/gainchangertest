using MediatR;

namespace BusinessLogic.Commands.Customers
{
    public class DeleteCustomerCommand :IRequest<bool>
    {
        public int CustomerId { get; }

        public DeleteCustomerCommand(int customerId)
        {
            CustomerId = customerId;
        }
    }
}
using System.Threading;
using System.Threading.Tasks;
using DataTransferObjects;
using MediatR;

namespace BusinessLogic.Commands.Customers
{
    public class SaveCustomerCommand : IRequest<bool>
    {
        public CustomerDto Customer { get; }
        public SaveCustomerCommand(CustomerDto customer)
        {
            this.Customer = customer;

        }
    }
}
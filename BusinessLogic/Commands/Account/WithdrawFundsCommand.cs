using MediatR;

namespace BusinessLogic.Commands.Account
{
    public class WithdrawFundsCommand : IRequest<bool>
    {
        public int CustomerId { get; }
        public decimal Funds { get; }
        public WithdrawFundsCommand(int customerId, decimal funds)
        {
            CustomerId = customerId;
            Funds = funds;
        }
    }
}
using MediatR;

namespace BusinessLogic.Commands.Account
{
    public class DepositFundsCommand : IRequest<bool>
    {
        public int CustomerId { get; }
        public decimal Funds { get; }
        public DepositFundsCommand(int customerId, decimal funds)
        {
            CustomerId = customerId;
            Funds = funds;
        }
    }
}
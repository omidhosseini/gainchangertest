using System.Threading;
using System.Threading.Tasks;
using BusinessLogic.Queries.Customers;
using DataTransferObjects;

using MediatR;


namespace BusinessLogic.Handlers.Customers
{
    public class GetCustomerHandler : IRequestHandler<GetCustomerQuery, CustomerDto>
    {
        private readonly ICustomersManager _customerManager;
        public GetCustomerHandler(ICustomersManager customersManager)
        {
            _customerManager = customersManager;
        }
        public Task<CustomerDto> Handle(GetCustomerQuery request, CancellationToken cancellationToken)
        {
            return Task.FromResult(_customerManager.GetCustomer(request.CustomerId));
        }
    }
}
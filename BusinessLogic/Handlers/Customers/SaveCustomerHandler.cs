using System.Threading;
using System.Threading.Tasks;
using BusinessLogic.Commands.Customers;
using DataTransferObjects;
using MediatR;

namespace BusinessLogic.Handlers.Customers
{
    public class SaveCustomerHandler : IRequestHandler<SaveCustomerCommand, bool>
    {
        private readonly ICustomersManager _customerManager;

        public SaveCustomerHandler(ICustomersManager customerManager)
        {
            _customerManager = customerManager;
        }

        public Task<bool> Handle(SaveCustomerCommand request, CancellationToken cancellationToken)
        {
            _customerManager.SaveCustomer(request.Customer);
            return Task.FromResult(true);
        }
    }
}
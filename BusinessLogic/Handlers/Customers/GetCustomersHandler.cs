using System.Collections;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using BusinessLogic.Queries.Customers;
using DataTransferObjects;
using MediatR;
using Repository;

namespace BusinessLogic.Handlers.Customers
{
    public class GetCustomersHandler : IRequestHandler<GetCustomersQuery, IEnumerable<CustomerDto>>
    {
        private readonly ICustomersManager _customerManager;
        public GetCustomersHandler(ICustomersManager customersManager)
        {
            _customerManager = customersManager;
        }
        public Task<IEnumerable<CustomerDto>> Handle(GetCustomersQuery request, CancellationToken cancellationToken)
        {
            return Task.FromResult(_customerManager.GetCustomers(request.PageSize, request.PageNumber));

        }
    }
}
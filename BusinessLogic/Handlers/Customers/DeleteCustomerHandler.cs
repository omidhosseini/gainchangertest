using System.Threading;
using System.Threading.Tasks;
using BusinessLogic.Commands.Customers;
using MediatR;

namespace BusinessLogic.Handlers.Customers
{
    public class DeleteCustomerHandler : IRequestHandler<DeleteCustomerCommand, bool>
    {
        private readonly ICustomersManager _customerManager;

        public DeleteCustomerHandler(ICustomersManager customerManager)
        {
            _customerManager = customerManager;
        }

        public Task<bool> Handle(DeleteCustomerCommand request, CancellationToken cancellationToken)
        {
            _customerManager.DeleteCustomer(request.CustomerId);
            return Task.FromResult(true);
        }
    }
}
using System.Threading;
using System.Threading.Tasks;
using BusinessLogic.Queries.Account;
using MediatR;

namespace BusinessLogic.Handlers.Account
{
    public class GetAvailableFundsHandler : IRequestHandler<GetAvailableFundsQuery, decimal>
    {
        private readonly IAccountService _accountService;

        public GetAvailableFundsHandler(IAccountService accountService)
        {
            _accountService = accountService;
        }

        public Task<decimal> Handle(GetAvailableFundsQuery request, CancellationToken cancellationToken)
        {
            return Task.FromResult(_accountService.GetAvailableFunds(request.CustomerId));
        }
    }
}
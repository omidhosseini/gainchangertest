using System.Threading;
using System.Threading.Tasks;
using BusinessLogic.Commands.Account;
using MediatR;

namespace BusinessLogic.Handlers.Account
{
    public class WithdrawFundsHandler : IRequestHandler<WithdrawFundsCommand, bool>
    {
        private readonly IAccountService _accountService;

        public WithdrawFundsHandler(IAccountService accountService)
        {
            _accountService = accountService;
        }

        public Task<bool> Handle(WithdrawFundsCommand request, CancellationToken cancellationToken)
        {
            _accountService.WithdrawFunds(request.CustomerId, request.Funds);
            return Task.FromResult(true);
        }
    }
}
using System.Threading;
using System.Threading.Tasks;
using BusinessLogic.Commands.Account;
using MediatR;

namespace BusinessLogic.Handlers.Account
{
    public class DepositFundsHandler : IRequestHandler<DepositFundsCommand, bool>
    {
        private readonly IAccountService _accountService;

        public DepositFundsHandler(IAccountService accountService)
        {
            _accountService = accountService;
        }

        public Task<bool> Handle(DepositFundsCommand request, CancellationToken cancellationToken)
        {
            _accountService.DepositFunds(request.CustomerId, request.Funds);
            return Task.FromResult(true);
        }
    }
}